# FreshRSS Dark pink theme

[Dark pink theme](https://gitlab.com/miicat/freshrss-dark-pink-theme) based on **Alternative Dark** theme made by Ghost for [FreshRSS](https://github.com/FreshRSS/FreshRSS)

![screenshot](https://gitlab.com/miicat/freshrss-dark-pink-theme/-/raw/main/Dark-pink/thumbs/original.png)

## Installation

This theme has been added to the offical release, so you use this theme as normal

1. Login to your FreshRSS
2. Go to: Settings -> Display -> Theme
3. Choose the **Dark pink - By: Miicat_47** -theme

### Docker

1. Copy the `Dark-pink` folder next to the FreshRSS's `docker-compose.yml`
2. Add following `- ./Dark-pink:/var/www/FreshRSS/p/themes/Dark-pink` line under `volumes:` in `docker-compose.yml`
3. Restart the FreshRSS docker container
4. Go to: Settings -> Display -> Theme
5. Choose the **Dark pink - By: Miicat_47** -theme

## Donating

If you want to support me, feel free to use PayPal
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://paypal.me/miicat)

## License

This theme is licensed under GPLv3
